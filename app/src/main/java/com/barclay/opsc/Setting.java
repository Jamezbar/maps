package com.barclay.opsc;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.ToggleButton;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class Setting extends AppCompatActivity
{
    FirebaseDatabase db;
    String nme;
    ToggleButton btUnit;
    RadioButton btCar,btWalk,btTransit;
    Button btUpdate;
    String key;
    String name;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        db = FirebaseDatabase.getInstance();
        nme = getIntent().getStringExtra("MailAddr");
        btUnit = findViewById(R.id.toggleButton);
        btCar = findViewById(R.id.radioButton2);
        btWalk = findViewById(R.id.radioButton3);
        btTransit = findViewById(R.id.radioButton);
        String[] str = nme.split("@");
        name = str[0];

        SetUp(name);
        btUpdate = findViewById(R.id.btUpdate);
        btUpdate.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                DatabaseReference ref = db.getReference("User");
                Query unitQuery  = ref.orderByChild("name").equalTo(name);
                unitQuery.addListenerForSingleValueEvent(new ValueEventListener()
                {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot)
                    {
                        if(dataSnapshot.exists())
                        {
                            for (DataSnapshot ds : dataSnapshot.getChildren()) {

                                key = ds.getKey();
                                System.out.println(key);
                                WriteSettings(key);
                            }
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError)
                    {

                    }
                });
            }
        });


    }

    private void WriteSettings(String key)
    {
        String unit = btUnit.isChecked() ? "imperial" : "metric";

        DatabaseReference usersRef = db.getReference("User/" + key);
        usersRef.child("Unit").setValue(unit);

        String mode;

        if(btCar.isChecked()){
            mode = "driving";
        }else if(btWalk.isChecked()){
            mode = "walking";
        }
        else{
            mode = "transit";
        }
        usersRef.child("mode").setValue(mode);

    }

    private void SetUp(String nme)
    {
        DatabaseReference ref = db.getReference("User");
        Query unitQuery  = ref.orderByChild("name").equalTo(nme);
        unitQuery.addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                if(dataSnapshot.exists())
                {
                    for (DataSnapshot ds : dataSnapshot.getChildren())
                    {
                        String ut = ds.child("Unit").getValue().toString();
                        if(ut.equalsIgnoreCase("metric"))
                        {
                            btUnit.setChecked(false);
                        }
                        if(ut.equalsIgnoreCase("Imperial"))
                        {
                            btUnit.setChecked(true);
                        }
                        String md = ds.child("mode").getValue().toString();
                        if(md.equalsIgnoreCase("driving"))
                        {
                            btCar.toggle();
                        }else if(md.equalsIgnoreCase("walking")){
                            btWalk.toggle();
                        }else{
                            btTransit.toggle();
                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {

            }
        });

    }

}
