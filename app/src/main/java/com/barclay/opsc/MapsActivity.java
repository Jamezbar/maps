package com.barclay.opsc;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;


import com.akexorcist.googledirection.DirectionCallback;
import com.akexorcist.googledirection.GoogleDirection;
import com.akexorcist.googledirection.model.Direction;
import com.akexorcist.googledirection.model.Info;
import com.akexorcist.googledirection.model.Leg;
import com.akexorcist.googledirection.model.Route;
import com.akexorcist.googledirection.util.DirectionConverter;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.*;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.PlaceDetectionClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;


import java.util.ArrayList;


public class MapsActivity extends AppCompatActivity  implements OnMapReadyCallback
{
    private static final String TAG = MapsActivity.class.getSimpleName();
    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    private GoogleMap myMap;
    private boolean LocPerm;
    private Location latLoc;
    private final LatLng DefLoc = new LatLng(-33.8523341, 151.2106085);
    private static final int DEFAULT_ZOOM = 15;
    private FusedLocationProviderClient locProviderClient;
    private GeoDataClient mGeoDataClient;
    private PlaceDetectionClient mPlaceDetectionClient;
    private static final String KEY_CAMERA_POSITION = "camera_position";
    private static final String KEY_LOCATION = "location";
    int PLACE_PICKER_REQUEST = 1;
    private Place destination;
    String unit = "";
    String mode = "walking";
    String nme;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps2);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.


        mGeoDataClient = Places.getGeoDataClient(this);

        mPlaceDetectionClient = Places.getPlaceDetectionClient(this);

        locProviderClient = LocationServices.getFusedLocationProviderClient(this);
        nme =getIntent().getStringExtra("Mailaddr");

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if (item.getItemId() == R.id.Settings) {
            String cUser = getIntent().getStringExtra("MailAddr");
            Intent intent = new Intent(getBaseContext(),Setting.class);
            intent.putExtra("MailAddr",cUser);
            startActivity(intent);
        }
        if (item.getItemId() == R.id.place) {
            openPlacePicker();
        }
        return true;
    }

    public void setunit(String ut){
        this.unit = ut;
    }

    public void setmode(String md){
        this.mode = md;
    }

    private void openPlacePicker()
    {

        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        try {
            startActivityForResult(builder.build(this), PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode,Intent data)
    {
        if(requestCode == PLACE_PICKER_REQUEST)
        {
            if (resultCode == RESULT_OK)
            {
               Place place = PlacePicker.getPlace(this,data);
               myMap.addMarker(new MarkerOptions().position(place.getLatLng()));
               SaveTrip(place);
               Navigate(place);
            }
        }
    }

    private void SaveTrip(Place place)
    {

    }

    private void Navigate(Place place)
    {
        getDeviceLocation();
        LatLng origin = new LatLng(latLoc.getLatitude(),latLoc.getLongitude());
        LatLng destination = place.getLatLng();
        getmode();
        getUnit();
        String key = "AIzaSyDIysh2meF_Iwv9vOZ_-c9AzUSOYgmYpLg";
        GoogleDirection.withServerKey(key).from(origin).to(destination).transportMode(mode).unit(getUnit()).execute(new DirectionCallback()
        {
            @Override
            public void onDirectionSuccess(Direction direction, String s)
            {
                if(direction.isOK())
                {
                    Route route = direction.getRouteList().get(0);
                    Leg leg = route.getLegList().get(0);
                    Info distanceInfo = leg.getDistance();
                    Info durationInfo = leg.getDuration();
                    String distance = distanceInfo.getText();
                    String duration = durationInfo.getText();
                    AlertDialog.Builder builder = new AlertDialog.Builder(MapsActivity.this);
                    builder.setMessage("The Distance of your trip is " + distance  + "\nYour trip will take " + duration ).setTitle("Trip Details");
                    AlertDialog log = builder.create();
                    log.show();
                    ArrayList<LatLng> directionPositionList = leg.getDirectionPoint();
                    PolylineOptions polylineOptions = DirectionConverter.createPolyline(MapsActivity.this,directionPositionList,5,Color.RED);
                    myMap.addPolyline(polylineOptions);
                }else
                {
                    System.out.println(direction.getStatus() + " " + direction.getErrorMessage());
                }


            }

            @Override
            public void onDirectionFailure(Throwable throwable)
            {
                System.out.println("lalalalalalalalalalalalalalalalalalala");
            }
        });



        //System.out.println(md.getDistanceValue(doc));

    }

    private String getUnit()
    {
        final String[] unt = new String[1];
        String em = getIntent().getStringExtra("MailAddr");
        String[] str = em.split("@");
        String user = str[0];
        FirebaseDatabase db = FirebaseDatabase.getInstance();
        DatabaseReference ref = db.getReference("User");
        Query unitQuery  = ref.orderByChild("name").equalTo(user);
        unitQuery.addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                if(dataSnapshot.exists())
                {
                    for (DataSnapshot ds : dataSnapshot.getChildren())
                    {
                        String ut = ds.child("Unit").getValue().toString();
                        unt[0] = ut;
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {

            }
        });

        return unt[0];
    }

    private void getmode()
    {
        String em = getIntent().getStringExtra("MailAddr");
        String[] str = em.split("@");
        String user = str[0];
        FirebaseDatabase db = FirebaseDatabase.getInstance();
        DatabaseReference ref = db.getReference("User");
        Query unitQuery  = ref.orderByChild("name").equalTo(user);
        unitQuery.addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                if(dataSnapshot.exists())
                {
                    for (DataSnapshot ds : dataSnapshot.getChildren())
                    {
                       String md = ds.child("mode").getValue().toString();
                        setmode(md);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {

            }
        });

    }


    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        myMap = googleMap;

        myMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter()
        {

            @Override
            // Return null here, so that getInfoContents() is called next.
            public View getInfoWindow(Marker arg0)
            {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker)
            {
                // Inflate the layouts for the info window, title and snippet.
                View infoWindow = getLayoutInflater().inflate(R.layout.custom_info_contents,
                        (FrameLayout) findViewById(R.id.map), false);

                TextView title = ((TextView) infoWindow.findViewById(R.id.title));
                title.setText(marker.getTitle());

                TextView snippet = ((TextView) infoWindow.findViewById(R.id.snippet));
                snippet.setText(marker.getSnippet());

                return infoWindow;
            }
        });
        getLocationPermission();

        // Turn on the My Location layer and the related control on the map.
        updateLocationUI();

        // Get the current location of the device and set the position of the map.
        getDeviceLocation();
    }


    private void getDeviceLocation()
    {
        try {
            if (LocPerm) {
                Task<Location> locationResult = locProviderClient.getLastLocation();
                locationResult.addOnCompleteListener(this, new OnCompleteListener<Location>()
                {
                    @Override
                    public void onComplete(@NonNull Task<Location> task)
                    {
                        if (task.isSuccessful()) {
                            // Set the map's camera position to the current location of the device.
                            latLoc = task.getResult();
                            myMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                                    new LatLng(latLoc.getLatitude(),
                                            latLoc.getLongitude()), DEFAULT_ZOOM));
                            myMap.addMarker(new MarkerOptions().position(new LatLng(latLoc.getLatitude(), latLoc.getLongitude())));

                        } else {
                            Log.d(TAG, "Current location is null. Using defaults.");
                            Log.e(TAG, "Exception: %s", task.getException());
                            myMap.moveCamera(CameraUpdateFactory
                                    .newLatLngZoom(DefLoc, DEFAULT_ZOOM));
                            myMap.getUiSettings().setMyLocationButtonEnabled(false);
                        }
                    }
                });
            }
        } catch (SecurityException e) {
            Log.e("Exception: %s", e.getMessage());
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */


    private void getLocationPermission()
    {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocPerm = true;
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }

    /**
     * Handles the result of the request for location permissions.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults)
    {
        LocPerm = false;
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    LocPerm = true;
                }
            }
        }
        updateLocationUI();
    }


    private void updateLocationUI()
    {
        if (myMap == null) {
            return;
        }
        try {
            if (LocPerm) {
                myMap.setMyLocationEnabled(true);
                myMap.getUiSettings().setMyLocationButtonEnabled(true);
            } else {
                myMap.setMyLocationEnabled(false);
                myMap.getUiSettings().setMyLocationButtonEnabled(false);
                latLoc = null;
                getLocationPermission();
            }
        } catch (SecurityException e) {
            Log.e("Exception: %s", e.getMessage());
        }
    }
}



